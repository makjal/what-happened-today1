package edu.example.whathappenedtoday;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity implements OnTouchListener {

	Button button1, button2, button3, button4;
	InputStream inputStream;
	String line,l12;
	TextView tv1;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        tv1 = (TextView)findViewById(R.id.textView1);
        readTxt2();
        
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        
        button1.setOnTouchListener(this);
        button2.setOnTouchListener(this);
        button3.setOnTouchListener(this);
        button4.setOnTouchListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
		switch(v.getId())
					
		{
        case R.id.button1:
	
        	//MainActivity.this.startActivity(new Intent(MainActivity.this,DisplayActivity.class));
        	String val1 = "famousbirthday";
        	Intent anotherActivityIntent1 = new Intent(this, DisplayActivity.class);
    		anotherActivityIntent1.putExtra("mydata", val1);
    		//startActivity(anotherActivityIntent1);
    		startActivityForResult(anotherActivityIntent1, 0);
    		break;
		
		case R.id.button2:
		
			String val2 = "famousdeaths";
        	Intent anotherActivityIntent2 = new Intent(this, DisplayActivity.class);
    		anotherActivityIntent2.putExtra("mydata", val2);
    		startActivity(anotherActivityIntent2);
    		break;
			
		case R.id.button3:
			
			String val3 = "historicalevents";
        	Intent anotherActivityIntent3 = new Intent(this, DisplayActivity.class);
    		anotherActivityIntent3.putExtra("mydata", val3);
    		startActivity(anotherActivityIntent3);
    		break;
        
		case R.id.button4:
			
			String val4 = "computerhistory";
        	Intent anotherActivityIntent4 = new Intent(this, DisplayActivity.class);
    		anotherActivityIntent4.putExtra("mydata", val4);
    		startActivity(anotherActivityIntent4);
    		break;
        	
		}
		return false;
	}
	
	   
		 public void readTxt2(){
		        
			 try{

				 
				   inputStream = getResources().openRawResource(R.raw.indianworld);

				   BufferedReader buffreader = new BufferedReader(new InputStreamReader(inputStream));

				   ArrayList<String> lines = new ArrayList<String>();
			            boolean hasNextLine =true;
			            while (hasNextLine){
			            	
			                line =  buffreader.readLine();
			      
			                SimpleDateFormat dfDate_day= new SimpleDateFormat("dd-MM");
			    		    String dt="";
			    		    Calendar c = Calendar.getInstance(); 
			    		    dt=dfDate_day.format(c.getTime());
			    		    l12 = line.substring(0, 5);
			    		    System.out.println(l12);
			              if(l12.equals(dt))
			                {
			                lines.add(line);
			                tv1.setText(line);
			                }
			                hasNextLine = line != null;

			            }

			        inputStream.close();

			    }
			     catch(Exception e){
			    	 System.out.println("Exception caught4");
			    	 e.printStackTrace();
			    	 
			     }
			}
}
